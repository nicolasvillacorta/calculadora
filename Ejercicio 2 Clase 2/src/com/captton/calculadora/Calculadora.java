package com.captton.calculadora;

public class Calculadora {
	public int sumar(int a, int b) {
		return a+b;
	}
	
	public int sumar(int a, int b, int c) {
		return a+b+c;
	}
	
	public int restar(int a, int b) {
		return a-b;
	}
	
	public int restar(int a, int b, int c) {
		return a-b-c;
	}
	
	public int multiplicar(int a, int b) {
		return a*b;
	}
	
	public int multiplicar(int a, int b, int c) {
		return a*b*c;
	}
	
	public float dividir(float a, int b) {
		return a/b;
	}

	public Calculadora() {
		
	}
}

