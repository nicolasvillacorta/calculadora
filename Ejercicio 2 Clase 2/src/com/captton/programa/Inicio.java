package com.captton.programa;
import com.captton.calculadora.Calculadora;


public class Inicio {

	public static void main(String[] args) {
		
		Calculadora calcu = new Calculadora();
		System.out.println(calcu.sumar(2, 4));
		System.out.println(calcu.sumar(2, 5, 7));
		System.out.println(calcu.restar(2, 4));
		System.out.println(calcu.restar(2, 5, 7));
		System.out.println(calcu.multiplicar(2, 4));
		System.out.println(calcu.multiplicar(2, 5, 7));
		System.out.println(calcu.dividir(9, 2));
	}

}
